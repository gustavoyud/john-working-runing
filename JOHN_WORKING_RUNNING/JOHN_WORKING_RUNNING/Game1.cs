using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace JOHN_WORKING_RUNNING
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        /// <summary>
        /// Root folder
        /// </summary>
        private const string AssetName = "JohnSprite";

        /// <summary>
        /// Graphics
        /// </summary>
        GraphicsDeviceManager graphics;

        /// <summary>
        /// Sprite batch
        /// </summary>
        SpriteBatch spriteBatch;

        /// <summary>
        /// Character
        /// </summary>
        John Jonh;

        /// <summary>
        /// Background
        /// </summary>
        Terreno background;

        /// <summary>
        /// Up Box
        /// </summary>
        Caixa upBox;

        /// <summary>
        /// Down Box
        /// </summary>
        Caixa DownBox;

        /// <summary>
        /// Scoreboard Instance
        /// </summary>
        Scoreboard scoreboard;

        /// <summary>
        /// Verifies if user is slowed
        /// </summary>
        Boolean isSlowed = false;
        
        /// <summary>
        /// State Machine
        /// </summary>
        public enum StateMachine { GameRunning, Pause, GameOver }

        /// <summary>
        /// Game State
        /// </summary>
        private StateMachine gameState = StateMachine.GameRunning;

        /// <summary>
        /// Game Over Class
        /// </summary>
        private GameOver gameover;

        /// <summary>
        /// Pause
        /// </summary>
        private Pause pause;
        
        /// <summary>
        /// Timeout
        /// </summary>
        private DateTime timeout = DateTime.Now;

        /// <summary>
        /// Runner Timeout
        /// </summary>
        private DateTime runnerTimeout = DateTime.Now;

        /// <summary>
        /// List all timer ( Character Life ) Objects
        /// </summary>
        private List<CharacterLife> timerList = new List<CharacterLife>();

        /// <summary>
        /// Current speed
        /// </summary>
        private int currentValueSpeed = 2;

        /// <summary>
        /// Background music
        /// </summary>
        private SoundEffect gameMusic;
        
        /// <summary>
        /// Background music instance
        /// </summary>
        private SoundEffectInstance gameMusicInstance;

        /// <summary>
        /// Box Colission Effect
        /// </summary>
        private SoundEffect boxEffect;

        /// <summary>
        /// Box Colission instance
        /// </summary>
        private SoundEffectInstance boxEffectInstance;

        /// <summary>
        /// Lose Effect
        /// </summary>
        private SoundEffect loseEffect;

        /// <summary>
        /// Lose Effect Instance
        /// </summary>
        private SoundEffectInstance loseEffectInstance;

        /// <summary>
        /// Constructor
        /// </summary>
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Jonh = new John(this);
            background = new Terreno(this);
            scoreboard = new Scoreboard(this);
            upBox = new Caixa(this, new Point(700, 350), 750, 850, new Point(700, 350));
            DownBox = new Caixa(this, new Point(900, 420), 900, 1200, new Point(900, 420 + 25));
            gameover = new GameOver(this, new Point(0, 0));
            Content.RootDirectory = "Content";
            pause = new Pause(this, new Point(0, 0));

            if (gameState == StateMachine.GameRunning)
                createTimerList();
        }

        /// <summary>
        /// Create Timer
        /// </summary>
        private void createTimerList()
        {
            for (int i = 0; i < 3; i++)
                timerList.Add(new CharacterLife(this, new Point(280 + (40 * i), 255)));
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Jonh.Initialize();
            background.Initialize();
            upBox.Initialize();
            DownBox.Initialize();
            scoreboard.Initialize();
            gameover.Initialize();
            pause.Initialize();

            if (gameState == StateMachine.GameRunning)
                initializeList();
            base.Initialize();
        }

        /// <summary>
        /// Initialize List
        /// </summary>
        private void initializeList()
        {
            for (int i = 0; i < timerList.Count; i++)
                timerList[i].Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            background.LoadContent(this);
            Jonh.LoadContent(this);
            upBox.LoadContent(this);
            DownBox.LoadContent(this);
            scoreboard.LoadContent(this);
            gameover.LoadContent(this);
            pause.LoadContent(this);

            // Load Music
            gameMusic = Content.Load<SoundEffect>("backgroundMusic");
            gameMusicInstance = gameMusic.CreateInstance();

            boxEffect = Content.Load<SoundEffect>("boxSound");
            boxEffectInstance = boxEffect.CreateInstance();

            loseEffect = Content.Load<SoundEffect>("Lose");
            loseEffectInstance = loseEffect.CreateInstance();

            if (gameState == StateMachine.GameRunning)
                loadListContent();
        }

        /// <summary>
        /// Load content of list
        /// </summary>
        private void loadListContent()
        {
            for (int i = 0; i < timerList.Count; i++)
            {
                spriteBatch = new SpriteBatch(GraphicsDevice);
                timerList[i].LoadContent(this);
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyBoardEvents();
            Jonh.Update(gameTime);
            background.Update(gameTime);
            upBox.Update(gameTime);
            DownBox.Update(gameTime);
            scoreboard.Update(gameTime);
            gameover.Update(gameTime);

            if (gameState == StateMachine.GameRunning)
            {
                if (loseEffectInstance.State != SoundState.Stopped)
                    loseEffectInstance.Stop();

                if (gameMusicInstance.State != SoundState.Playing)
                    gameMusicInstance.Play();

                updateList(gameTime);
                infiniteRunningEvents();
                boundingBoxIntersects();
            } else if ( gameState == StateMachine.Pause)
            {
                pause.Update(gameTime);
                if (gameMusicInstance.State != SoundState.Paused)
                    gameMusicInstance.Pause();
            } else if ( gameState == StateMachine.GameOver)
            {
                if (gameMusicInstance.State != SoundState.Stopped)
                    gameMusicInstance.Stop();

                if (loseEffectInstance.State != SoundState.Playing)
                    loseEffectInstance.Play();
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// Update method for list
        /// </summary>
        /// <param name="gameTime"></param>
        private void updateList(GameTime gameTime)
        {
            for (int i = 0; i < timerList.Count; i++)
                timerList[i].Update(gameTime);
        }


        /// <summary>
        /// KeyBoard Events
        /// </summary>
       private void KeyBoardEvents()
        {
            pauseGame();
            switch (gameState)
            {
                case StateMachine.GameOver:
                    ResetGame();
                    break;

                case StateMachine.GameRunning:
                    JohnKeyInteracts();
                    break;

                case StateMachine.Pause:
                    closeGame();
                    background.backgroundSpeed = 0;
                    DownBox.boxSpeed = 0;
                    upBox.boxSpeed = 0;
                    Jonh.estado = John.Estados.Idle;
                    break;
            }
        }

        /// <summary>
        /// Verify pressed keys to pause the game
        /// </summary>
        private void pauseGame()
        {
            switch(gameState)
            {
                case StateMachine.GameRunning:
                    gamePause();
                    break;

                case StateMachine.Pause:
                    gameContinue();
                    break;
            }

        }

        /// <summary>
        /// Pause Current Game
        /// </summary>
        private void gamePause()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                gameState = StateMachine.Pause;
                currentValueSpeed = background.backgroundSpeed;
            }
        }

        /// <summary>
        /// Continue Paused game
        /// </summary>
        private void gameContinue()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                gameState = StateMachine.GameRunning;
                background.backgroundSpeed = currentValueSpeed;
                DownBox.boxSpeed = currentValueSpeed;
                upBox.boxSpeed = currentValueSpeed;
                Jonh.estado = John.Estados.Andando;
            }
        }   

        /// <summary>
        /// Closes game
        /// </summary>
        private void closeGame()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Q))
                this.Exit();

        }

        /// <summary>
        /// Reset game
        /// </summary>
        private void ResetGame()
        {
            if(Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                gameState = StateMachine.GameRunning;
                background.backgroundSpeed = 2;
                DownBox.boxSpeed = 2;
                upBox.boxSpeed = 2;
                upBox.Posicao = new Point(700, 350);
                Jonh.estado = John.Estados.Andando;
                scoreboard.gameOver = false;
                createTimerList();
                Initialize();
                LoadContent();
                DownBox.Posicao = new Point(900, 420);
                runnerTimeout = DateTime.Now;

                if (loseEffectInstance.State != SoundState.Stopped)
                    loseEffectInstance.Stop();
            }
        }

        /// <summary>
        /// Keys to move the character
        /// </summary>
        private void JohnKeyInteracts()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
                Jonh.Mover(John.Direcoes.Cima);
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
                Jonh.Mover(John.Direcoes.Baixo);
        }

        /// <summary>
        /// Events needed to make a runner game
        /// </summary>
        private void infiniteRunningEvents()
        {
            background.Mover(Terreno.Direcoes.Esquerda);
            upBox.Mover(Caixa.Direcoes.Esquerda);
            DownBox.Mover(Caixa.Direcoes.Esquerda);
            
            if (DateTime.Now > runnerTimeout + TimeSpan.FromSeconds(11))
            {
                background.backgroundSpeed++;
                upBox.boxSpeed++;
                DownBox.boxSpeed++;
                Jonh.spriteDelay -= 3;
                runnerTimeout = DateTime.Now;
            }

            if (timerList.Count == 0)
            {
                gameState = StateMachine.GameOver;
                background.backgroundSpeed = 0;
                DownBox.boxSpeed = 0;
                upBox.boxSpeed = 0;
                Jonh.estado = John.Estados.Idle;
                scoreboard.gameOver = true;
            }
        }

        /// <summary>
        /// Bounding box intersects
        /// </summary>
        private void boundingBoxIntersects()
        {
            if (
                Jonh.BoundingBox.Intersects(background.BoundingBox) && 
                gameState == StateMachine.GameRunning
                )
                Jonh.Posicao = new Point(Jonh.Posicao.X, 320);
            applySlowedEffect();
            
        }

        /// <summary>
        /// Decrements time
        /// </summary>
        private void applySlowedEffect()
        {
            if (
                (Jonh.BoundingBox.Intersects(upBox.BoundingBox) ||
                Jonh.BoundingBox.Intersects(DownBox.BoundingBox)) &&
                 gameState == StateMachine.GameRunning
                )
            {

                    if (!isSlowed)
                {
                    isSlowed = true;
                    timeout = DateTime.Now;
                    timerList.Remove(timerList[timerList.Count - 1]);
                    if (boxEffectInstance.State != SoundState.Playing)
                        boxEffectInstance.Play();
                }
            }

            if (isSlowed)
            {
                if (DateTime.Now > timeout + TimeSpan.FromSeconds(2))
                {
                    Jonh.spriteDelay = 60;
                    isSlowed = false;
                }
            }

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {   
            switch (gameState)
            {
                case StateMachine.GameOver:
                    background.Draw(gameTime);
                    upBox.Draw(gameTime);
                    Jonh.Draw(gameTime);
                    DownBox.Draw(gameTime);
                    gameover.Draw(gameTime);
                    break;

                case StateMachine.GameRunning:
                    background.Draw(gameTime);
                    drawList(gameTime);
                    scoreboard.Draw(gameTime);
                    upBox.Draw(gameTime);
                    Jonh.Draw(gameTime);
                    DownBox.Draw(gameTime);
                    break;

                case StateMachine.Pause:
                    background.Draw(gameTime);
                    upBox.Draw(gameTime);
                    Jonh.Draw(gameTime);
                    DownBox.Draw(gameTime);
                    pause.Draw(gameTime);
                    break;
            }

            base.Draw(gameTime);
        }

        /// <summary>
        /// Draw Method from List
        /// </summary>
        /// <param name="gameTime"></param>
        private void drawList(GameTime gameTime)
        {
            for (int i = 0; i < timerList.Count; i++)
            {
                timerList[i].Draw(gameTime);
            }
        }
    }
}
