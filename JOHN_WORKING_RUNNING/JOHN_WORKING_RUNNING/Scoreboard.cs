using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace JOHN_WORKING_RUNNING
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Scoreboard : Microsoft.Xna.Framework.DrawableGameComponent
    {
        /// <summary>
        /// Texture
        /// </summary>
        private SpriteFont scoreboard;

        /// <summary>
        /// Title font
        /// </summary>
        private Texture2D title;

        /// <summary>
        /// Assets to be loaded name
        /// </summary>
        private const string AssetName = "scoreboard";

        /// <summary>
        /// Title asset name
        /// </summary>
        private const string TitleAssetName = "timePasted";

        /// <summary>
        /// Scoreboards value
        /// </summary>
        public int scoreboardValue;

        /// <summary>
        /// Sprite batch
        /// </summary>
        private SpriteBatch spriteBatch;

        /// <summary>
        /// Date time to calculate sprite font load delay
        /// </summary>
        private DateTime date;

        /// <summary>
        /// Verifies Game Over
        /// </summary>
        public Boolean gameOver = false;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="game"> Game Class </param>
        public Scoreboard(Game game)
            : base(game)
        {
            date = DateTime.Now;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
            scoreboardValue = 0;
        }

        /// <summary>
        /// Load Game contents
        /// </summary>
        /// <param name="game"></param>
        public void LoadContent(Game game)
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            scoreboard = game.Content.Load<SpriteFont>(AssetName);
            title = game.Content.Load<Texture2D>(TitleAssetName);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            if (DateTime.Now > date + TimeSpan.FromSeconds(1) && !gameOver)
            {
                scoreboardValue++;
                date = DateTime.Now;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// Drawable method
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(
                scoreboard,
                scoreboardValue.ToString() + "s",
                new Vector2(280, 78),
                Color.White
            );
            spriteBatch.Draw(
              title,
              new Rectangle(280, 75, title.Width, title.Height),
              Color.White
            );
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
