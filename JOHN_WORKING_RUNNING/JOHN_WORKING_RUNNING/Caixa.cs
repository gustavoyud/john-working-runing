using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace JOHN_WORKING_RUNNING
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Caixa : Microsoft.Xna.Framework.DrawableGameComponent
    {
        /// <summary>
        /// Direction
        /// </summary>
        public enum Direcoes {Direita, Esquerda }

        /// <summary>
        /// State
        /// </summary>
        public enum Estados { Idle, Andando }

        /// <summary>
        /// Texture
        /// </summary>
        public Texture2D caixa;

        /// <summary>
        /// Box Bounding Box
        /// </summary>
        public Rectangle BoundingBox { get; set; }

        /// <summary>
        /// Position
        /// </summary>
        public Point Posicao { get; set; }

        /// <summary>
        /// Assets to be loaded name
        /// </summary>
        private const string AssetName = "caixa-01";

        /// <summary>
        /// Random number to load boxes
        /// </summary>
        private Random boxPosition = new Random();

        /// <summary>
        /// Sprite batch
        /// </summary>
        private SpriteBatch spriteBatch;

        /// <summary>
        /// Directions Instance
        /// </summary>
        private Direcoes direcao;

        /// <summary>
        /// Position to be updated
        /// </summary>
        private Point positionToUpdate;

        /// <summary>
        /// Bounding Box Position
        /// </summary>
        private Point boundingBoxPosition;

        /// <summary>
        /// Random Initial Range Limit
        /// </summary>
        private int randomInitialRange;

        /// <summary>
        /// Random Final Range Limit
        /// </summary>
        private int randomFinalRange;

        /// <summary>
        /// Sprites sizes
        /// </summary>
        private Point spriteSize;

        /// <summary>
        /// box Speed
        /// </summary>
        public int boxSpeed;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="game"> Game Class </param>
        public Caixa(Game game)
            : base(game)
        {
            Posicao = new Point(700, 350);
        }

        /// <summary>
        /// Optional Constructor
        /// </summary>
        /// <param name="game"> Game Class </param>
        /// <param name="position"> Initial Position </param>
        /// <param name="randomInitialRange"> Initial Range </param>
        /// <param name="randomFinalRange"> Final Range </param>
        public Caixa(Game game, Point position, int randomInitialRange, int randomFinalRange, Point BoundingBoxPosition)
           : base(game)
        {
            Posicao = position;
            positionToUpdate = position;
            spriteSize = new Point(50, 61);
            BoundingBox = new Rectangle(Posicao.X, Posicao.Y, spriteSize.X, spriteSize.Y);
            boundingBoxPosition = BoundingBoxPosition;
            this.randomInitialRange = randomInitialRange;
            this.randomFinalRange = randomFinalRange;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
            boxSpeed = 2;
        }

        /// <summary>
        /// Load Game contents
        /// </summary>
        /// <param name="game"></param>
        public void LoadContent(Game game)
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            caixa = game.Content.Load<Texture2D>(AssetName);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            if (Posicao.X <= -(boxPosition.Next(randomInitialRange, randomFinalRange)))
                Posicao = positionToUpdate;

            BoundingBox = new Rectangle(Posicao.X, boundingBoxPosition.Y, spriteSize.X, caixa.Height);
            base.Update(gameTime);
        }

        /// <summary>
        /// Drawable method
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
                spriteBatch.Draw(caixa,new Rectangle(Posicao.X, Posicao.Y, caixa.Width, caixa.Height),Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Move into provided directions
        /// </summary>
        /// <param name="position"> Provided Direction Argument </param>
       public void Mover(Direcoes position)
        {
            direcao = position;
            switch (position)
            {
                case Direcoes.Esquerda: Posicao = new Point(Posicao.X - (2 * boxSpeed), Posicao.Y); break;
            }
        }
    }
}
