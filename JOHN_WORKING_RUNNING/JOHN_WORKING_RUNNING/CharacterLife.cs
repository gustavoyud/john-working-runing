using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace JOHN_WORKING_RUNNING
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class CharacterLife : Microsoft.Xna.Framework.DrawableGameComponent
    {
        /// <summary>
        /// Texture
        /// </summary>
        private Texture2D time;

        /// <summary>
        /// Assets to be loaded name
        /// </summary>
        private const string AssetName = "circular-clock";

        /// <summary>
        /// Sprite batch
        /// </summary>
        private SpriteBatch spriteBatch;

        /// <summary>
        /// Timer position
        /// </summary>
        public Point timerPosition;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="game"> Game Class </param>
        public CharacterLife(Game game)
            : base(game)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="game"> Game Class </param>
        public CharacterLife(Game game, Point position)
            : base(game)
        {
            timerPosition = position;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Load Game contents
        /// </summary>
        /// <param name="game"></param>
        public void LoadContent(Game game)
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            time = game.Content.Load<Texture2D>(AssetName);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// Drawable method
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(
                time,
                new Rectangle(timerPosition.X, timerPosition.Y, time.Width, time.Height),
                Color.White
            );
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
